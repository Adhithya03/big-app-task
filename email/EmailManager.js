const express = require("express");
const router = express.Router();
const async = require("async");
const bodyParser = require("body-parser");
const EmailSVC = require("./emailService.js");
// const EmailConfig = require("./emailConfig.js");
const HttpStatus = require("http-status-codes");

router.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
// console.log(transporter);

/**
 * @api {post} /saveemailschedule
 * @apiVersion V1
 * @apiName saveemailschedule
 * @apiGroup Email
 *
 * @apiparams {saveSchedule}
 * {
    EmailTO: '',
    Subject: '',
    Body: '',
    Date: '',(YYYY-MM-DD)
    Time: '', (HH:MM:SS)
    }
 * @apiDesc  Used to Save Email Schedules into db.
 *
 * @apiSuccess {saveScheduleStatus}  Success Message will be sent.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 * {
      status: 'success'
    }
 *
 * @apiError Value Provided by User is Incorrect.
 *
 * @apiErrorExample Error-Response:
 *    HTTP/1.1 201 Not Found
 *    {
 *      success: 'false',
 *      message: 'Internal Server error'
 *    }
 * @apiImplementationSteps
 *   1. Make a call to EmailSVC to Save Email Schedules into db.
 *   2. IF sucessfully send succressResponse.
 *   3. IF Erorr Send Error-Response.
 */

router.post("/saveemailschedule", function (req, res) {
  const saveSchedule = req.body;
  let errorMessage = "";
  let saveScheduleStatus = "";
  async.series(
    [
      // Step-1 Make a call to EmailSVC to Save Email Schedules into db.
      function (callback) {
        EmailSVC.SaveSchedules(saveSchedule)
          .then(function (data) {
            callback(null, 2);
          })
          .catch(function (e) {
            // Step-3 IF Erorr Send Error-Response.
            errorMessage = { success: false, message: "Internal Server error" };
            res.status(HttpStatus.EXPECTATION_FAILED).send(errorMessage);
          });
      },
    ],
    function () {
      saveScheduleStatus = {
        status: "Success",
      };
      // Step-2 IF sucessfully send succressResponse.
      res.status(HttpStatus.OK).send(saveScheduleStatus);
    }
  );
});

/**
 * @api {get} /getallemailschedule
 * @apiVersion V1
 * @apiName getallemailschedule
 * @apiGroup Email
 *
 * @apiDesc  Used to Get all Email Schedules from db.
 *
 * @apiSuccess {ScheduleDetails}  Success Message will be sent.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 * {
        ScheduleID: '',
        EmailTO: '',
        Subject: '',
        Body: '',
        Date: '',
        Time: '',
        IsFailed: '',
    }
 *
 * @apiError Value Provided by User is Incorrect.
 *
 * @apiErrorExample Error-Response:
 *    HTTP/1.1 201 Not Found
 *    {
 *      success: 'false',
 *      message: 'Internal Server error'
 *    }
 * @apiImplementationSteps
 *   1. Make a call to EmailSVC to Get all Email Schedules from db.
 *   2. IF sucessfully send succressResponse.
 *   3. IF Erorr Send Error-Response.
 */

router.get("/getallemailschedule", function (req, res) {
  let ScheduleDetails = "";
  let errorMessage = "";
  async.series(
    [
      // Step-1 Make a call to EmailSVC to Get all Email Schedules from db.
      function (callback) {
        EmailSVC.GetAllSchedules()
          .then(function (data) {
            data.length === 0
              ? ((errorMessage = {
                  success: false,
                  message: "No Data Found",
                }),
                res.status(HttpStatus.EXPECTATION_FAILED).send(errorMessage))
              : ((ScheduleDetails = data), callback(null, 2));
          })
          .catch(function (e) {
            console.log(e);
            // Step-3 IF Erorr Send Error-Response.
            errorMessage = { success: false, message: "Internal Server error" };
            res.status(HttpStatus.EXPECTATION_FAILED).send(errorMessage);
          });
      },
    ],
    function () {
      // Step-2 IF sucessfully send succressResponse.
      res.status(HttpStatus.OK).send(ScheduleDetails);
    }
  );
});

/**
 * @api {put} /rescheduling
 * @apiVersion V1
 * @apiName rescheduling
 * @apiGroup Email
 *
 * @apiparams {ScheduleDetails}
 * {
    EmailTO: '',
    Subject: '',
    Body: '',
    Date: '', (YYYY-MM-DD)
    Time: '', (HH:MM:SS)
    }
 *
 * @apiparams {scheduleid} scheduleID
 *
 * @apiDesc  Used to Update Email Schedules into db.
 *
 * @apiSuccess {ScheduleStatus}  Success Message will be sent.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 * {
    status: 'Success'
    }
 *
 * @apiError Value Provided by User is Incorrect.
 *
 * @apiErrorExample Error-Response:
 *    HTTP/1.1 201 Not Found
 *    {
 *      success: 'false',
 *      message: 'Internal Server error'
 *    }
 * @apiImplementationSteps
 *   1. Make a call to EmailSVC to Update Email Schedules into db.
 *   2. IF sucessfully send succressResponse.
 *   3. IF Erorr Send Error-Response.
 */

router.put("/rescheduling/:scheduleid", function (req, res) {
  const updateScheduleDetails = req.body;
  const scheduleID = req.params.scheduleid;
  let errorMessage = "";
  let ScheduleStatus = "";
  async.series(
    [
      // Step-1 Make a call to EmailSVC to Update Email Schedules into db.
      function (callback) {
        EmailSVC.UpdateSchedules(updateScheduleDetails, scheduleID)
          .then(function (data) {
            callback(null, 2);
          })
          .catch(function (e) {
            // Step-3 IF Erorr Send Error-Response.
            errorMessage = { success: false, message: "Internal Server error" };
            res.status(HttpStatus.EXPECTATION_FAILED).send(errorMessage);
          });
      },
    ],
    function () {
      ScheduleStatus = {
        status: "Success",
      };
      // Step-2 IF sucessfully send succressResponse.
      res.status(HttpStatus.OK).send(ScheduleStatus);
    }
  );
});

/**
 * @api {delete} /deleteemailschedule
 * @apiVersion V1
 * @apiName deleteemailschedule
 * @apiGroup Email
 *
 *  @apiparams {scheduleid} scheduleID
 *
 * @apiDesc  Used to Delete Email Schedules from db.
 *
 * @apiSuccess {ScheduleStatus}  Success Message will be sent.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 * {
    status: 'Success'
    }
 *
 * @apiError Value Provided by User is Incorrect.
 *
 * @apiErrorExample Error-Response:
 *    HTTP/1.1 201 Not Found
 *    {
 *      success: 'false',
 *      message: 'Internal Server error'
 *    }
 * @apiImplementationSteps
 *   1. Make a call to EmailSVC to Delete Email Schedules from db.
 *   2. IF sucessfully send succressResponse.
 *   3. IF Erorr Send Error-Response.
 */

router.delete("/deleteemailschedule/:scheduleid", function (req, res) {
  const scheduleID = req.params.scheduleid;
  let errorMessage = "";
  let ScheduleStatus = "";
  async.series(
    [
      // Step-1 Make a call to EmailSVC to Delete Email Schedules from db.
      function (callback) {
        EmailSVC.DeleteSchedules(scheduleID)
          .then(function (data) {
            callback(null, 2);
          })
          .catch(function (e) {
            // Step-3 IF Erorr Send Error-Response.
            errorMessage = { success: false, message: "Internal Server error" };
            res.status(HttpStatus.EXPECTATION_FAILED).send(errorMessage);
          });
      },
    ],
    function () {
      ScheduleStatus = {
        status: "Success",
      };
      // Step-2 IF sucessfully send succressResponse.
      res.status(HttpStatus.OK).send(ScheduleStatus);
    }
  );
});

module.exports = router;

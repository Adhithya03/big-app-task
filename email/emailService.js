const cron = require("node-cron");
const db = require("../app_config/dbconfig.js");

/**
 *  @function SaveSchedules
 *
 *  @description Used to Save Email Schedules into schedule.
 */

const SaveSchedules = function (saveSchedule) {
  const saveSchedules =
    "INSERT INTO `schedule`(`EmailTO`, `Subject`, `Body`, `Date`, `Time`, `IsFailed`) VALUES ?";
  return new Promise(function (resolve, reject) {
    const scheduleDetails = {
      EmailTO: saveSchedule.EmailTO,
      Subject: saveSchedule.Subject,
      Body: saveSchedule.Body,
      Date: saveSchedule.Date,
      Time: saveSchedule.Time,
      IsFailed: 0,
    };
    const schedule = [
      [
        scheduleDetails.EmailTO,
        scheduleDetails.Subject,
        scheduleDetails.Body,
        scheduleDetails.Date,
        scheduleDetails.Time,
        scheduleDetails.IsFailed,
      ],
    ];
    db.query(saveSchedules, [schedule], function (err, data) {
      err ? (console.error(err), reject(err)) : resolve(data);
    });
  });
};

/**
 *  @function GetAllSchedules
 *
 *  @description Used to Get All Email Schedules into schedule.
 */

const GetAllSchedules = function () {
  const getSchedules = "select * from schedule;";
  console.log("if");
  return new Promise(function (resolve, reject) {
    db.query(getSchedules, [], function (err, data) {
      err ? (console.error(err), reject(err)) : console.log(data),
        resolve(data);
    });
  });
};

/**
 *  @function SaveSchedules
 *
 *  @description Used to Save Email Schedules into schedule.
 */

const UpdateSchedules = function (updateScheduleDetails, scheduleID) {
  const updateSchedules = "update `schedule` set ? where ScheduleID = ?";
  return new Promise(function (resolve, reject) {
    const scheduleDetails = {
      EmailTO: updateScheduleDetails.EmailTO,
      Subject: updateScheduleDetails.Subject,
      Body: updateScheduleDetails.Body,
      Date: updateScheduleDetails.Date,
      Time: updateScheduleDetails.Time,
    };
    db.query(
      updateSchedules,
      [scheduleDetails, scheduleID],
      function (err, data) {
        err ? (console.error(err), reject(err)) : resolve(data);
      }
    );
  });
};

/**
 *  @function DeleteSchedules
 *
 *  @description Used to Save Email Schedules into schedule.
 */

const DeleteSchedules = function (scheduleID) {
  const deleteSchedules = "delete from `schedule` where ScheduleID = ?";
  return new Promise(function (resolve, reject) {
    db.query(deleteSchedules, [scheduleID], function (err, data) {
      err ? (console.error(err), reject(err)) : resolve(data);
    });
  });
};

module.exports = {
  SaveSchedules,
  GetAllSchedules,
  UpdateSchedules,
  DeleteSchedules,
};

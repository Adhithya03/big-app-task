var app = require("./app");
const cron = require("node-cron");
const moment = require("moment");
const db = require("./app_config/dbconfig.js");
const sgMail = require("@sendgrid/mail");
var port = process.env.PORT || 4800;

var server = app.listen(port, function () {
  console.log("Express server listening on port " + port);
});

// Sending Mail
function emailSending(data, currentDate) {
  for (const i of data) {
    sgMail.setApiKey(
      "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"
    );
    const msg = {
      to: [i.EmailTO],
      from: "admin@adhi.com",
      templateId: "d-8bb9bdbb129849dab20bd03fe84e9ab0",
      dynamic_template_data: {
        subject: i.Subject,
        body: i.Body,
      },
    };
    console.log(msg);
    const response = sgMail.sendMultiple(msg);
    response.then((data) => console.log("email sent"));
    response.catch((error) => console.error(error.toString()));
  }
}

// Check Schedule Every Minute to Send Mail.
cron.schedule("* * * * *", () => {
  const getSchedules =
    "select * from schedule where Date = ? AND Time BETWEEN ? AND ?";
  const currentDate = moment().format("YYYY-MM-DD");
  const startTime = moment().format("kk:mm" + ":00");
  const endTime = moment().format("kk:mm" + ":59");
  console.log(currentDate);
  console.log(startTime);
  console.log(endTime);
  return new Promise(function (resolve, reject) {
    db.query(
      getSchedules,
      [currentDate, startTime, endTime],
      function (err, data) {
        console.log(data);
        err
          ? (console.error(err), reject(err))
          : data.length === 0
          ? null
          : emailSending(data, currentDate);
      }
    );
  });
});

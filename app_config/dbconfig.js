var mysql = require("mysql");
const dotenv = require("dotenv");
dotenv.config();
global.NODE_ENV = process.env.NODE_ENV;
console.log(global.NODE_ENV);
const mysqlConfig = require("../helpers/mysql.json");
let pool = "";
console.log(mysqlConfig.LOCAL);
if (global.NODE_ENV == "LOCAL") {
  pool = mysql.createPool({
    connectionLimit: mysqlConfig.LOCAL.connectionLimit,
    host: mysqlConfig.LOCAL.host,
    user: mysqlConfig.LOCAL.user,
    password: mysqlConfig.LOCAL.password,
    database: mysqlConfig.LOCAL.database,
    debug: mysqlConfig.LOCAL.debug,
  });
}

function executeQuery(sql, val, callback) {
  pool.getConnection((err, connection) => {
    if (err) {
      return callback(err, null);
    } else {
      if (connection) {
        connection.query(sql, val, function (error, results, fields) {
          connection.release();
          if (error) {
            return callback(error, null);
          }
          return callback(null, results);
        });
      }
    }
  });
}

function query(sql, val, callback) {
  executeQuery(sql, val, function (err, data) {
    if (err) {
      return callback(err);
    }
    callback(null, data);
  });
}

module.exports = {
  query: query,
};
